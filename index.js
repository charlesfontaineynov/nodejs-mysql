/*
Exercice 1

1. Ouvrir une base de donnée SQLite et créer une table :
- users (pseudo, email, firstname, lastname, createdAt, updatedAt)

1. Créer 6 routes
- ALL /
- POST /users
- GET /users/:userId
- GET /users?limit=20&offset=0
- DELETE /users/:userId
- PUT /users/:userId
autre

1. Implémenter chaque route dans l'ordre çi-dessus, vérifier le bon
fonctionnement à chaque fois. Les routes doivent répondre au
format JSON.

2. Bonus : Ajouter des filtres sur "/users" pour rechercher par
prénom, nom , ... et un paramètre de tri : "order=xxxx&reverse=1"
*/

const express = require('express')
const bodyParser = require('body-parser');
const mysql = require('mysql');

// listen from port 3000
const PORT = process.env.PORT || 3000;

// Create a connection to the database
const connexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ynov'
});

// initialize express
const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))

// connect to the BDD
connexion.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

// Get localhost:3000
app.get('/', function (req, res, next) {
    res.send('Hello World!');
    next();
});

// Get CREATE TABLE
app.get('/users/createtable', function(req, res, next) {
    let sql = "CREATE TABLE ynov.users (id smallint(5) unsigned NOT NULL AUTO_INCREMENT, pseudo VARCHAR(255) NOT NULL, email VARCHAR(40) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, createdAt DATETIME NOT NULL, updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    connexion.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
        console.log('Table created!');
    });
});

// GET /users/all (Prevent conflict with Get route GET /users/*?limit=20&offset=0)
app.get('/users/all', function (req, res, next) {
    let sql = "SELECT firstname, lastname FROM ynov.users";

    connexion.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
        console.log("Display all users!");
    });
});

// POST user
app.post('/users', (req, res) => {
    const sql = `INSERT INTO ynov.users (pseudo, email, firstname, lastname, createdAt) VALUES (?)`;
    const values = [
        req.body.pseudo,
        req.body.email,
        req.body.firstname,
        req.body.lastname,
        req.body.createdAt
    ];
    connexion.query(sql, [values], function (err, result){
        if (err) throw err;
        res.send('1 record inserted!');
        console.log("Number of records inserted: " + result.affectedRows + "!");
    })
});

// GET /users/:userId
app.get('/users/:id', (req, res) => {
    let sql = 'SELECT firstname, lastname FROM ynov.users WHERE id=' + req.params.id;

    connexion.query(sql, function(err, result){
        if (err) throw err;
        res.send(result);
        console.log("1 row displayed!");
    })
})

// GET /users/*?limit=20&offset=0
app.get('/users', function(req, res) {
    let startNum, endNum;

    if(req.query.limit === undefined && req.query.offset === undefined ){
        startNum = 0;
        endNum = 20;
    } else{
        startNum = parseInt(req.query.offset, 10);
        endNum = parseInt(req.query.limit, 10);
    }
    let sql = 'SELECT firstname, lastname FROM ynov.users LIMIT ' + endNum + ' OFFSET ' + startNum;

    connexion.query(sql, function(err, result) {
        if (err) throw err;
        res.send(result);
        console.log(result.length + " row(s) displayed!");
    })
});

// DELETE /users/:userId
app.delete('/users/:id', async function (req, res) {
    let sql = 'DELETE FROM ynov.users WHERE id=' + req.params.id + ' LIMIT 1';

    connexion.query(sql, function(err, result){
        if (err) throw err;
        res.send(result);
        console.log("1 row deleted!");
    })
});

// PUT /users/:userId
app.put('/users/:id', async function(req, res){
    let sql = 'UPDATE ynov.users SET firstname="Will", lastname="Smith" WHERE id=' + req.params.id;

    connexion.query(sql, function(err, result){
        if (err) throw err;
        res.send(result);
        console.log("1 row updated!");
        console.log(result.message);
    })
});

// listen for requests on PORT
app.listen(PORT, (error) => {
    if (error) console.log(error);
    console.log('Server is running on port', PORT);
});