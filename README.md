# NodeJS MySQL

Exercice 1

1. Ouvrir une base de donnée SQLite et créer une table :
- users (pseudo, email, firstname, lastname, createdAt, updatedAt)

1. Créer 6 routes
- ALL /
- POST /users
- GET /users/:userId
- GET /users?limit=20&offset=0
- DELETE /users/:userId
- PUT /users/:userId
autre

1. Implémenter chaque route dans l'ordre çi-dessus, vérifier le bon
fonctionnement à chaque fois. Les routes doivent répondre au
format JSON.

2. Bonus : Ajouter des filtres sur "/users" pour rechercher par
prénom, nom , ... et un paramètre de tri : "order=xxxx&reverse=1"